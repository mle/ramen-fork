import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import styled from 'styled-components'

const StyledView = styled.View`
  flex: 1;
  background-color: red;
  align-items: center;
  justify-content: center;
`

export default class App extends React.Component {
  render() {
    return (
      <StyledView>
        <Text>Ramen app</Text>
      </StyledView>
    );
  }
}
